-- ALTER TABLE `restaurant` CHANGE `menu_url` `menu_url` VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
ALTER TABLE restaurant ADD email varchar(100) NOT NULL;
CREATE UNIQUE INDEX restaurant_email_uindex ON restaurant (email);