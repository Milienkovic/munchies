$(document).ready(function () {

    // $('#send-email').css('visibility','hidden');
    $('.deleteRest').on('click', function (event) {
        var id = $(this).attr('value');
        /*<![CDATA[*/
        var path = /*[[@{/}]]*/'/restaurant/restaurantDetails/';
        /*]]>*/
        event.preventDefault();
        console.log($(this).attr('value'));

        bootbox.confirm({
            message: "Do you really want to delete restaurant?",
            buttons: {
                confirm: {
                    label: 'Confirm',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Cancel',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    console.log(location.pathname)
                    $.get(path + id + '/admin/deleteRestaurant/', {'id': id}, function (res) {
                        $(location).attr('href', '/restaurant/home')
                    });
                    console.log('This was logged in the callback: ' + result);
                }
            }
        });
    });
    console.log(location.host + 'host');

    disableFormElements();
    //refactor

    // $('#mlog').on('click',function (event) {
    //     $('#loginModal').modal();
    // });
    $('#fadeOutMessage').fadeOut(3000);

    // test();

    // checkOrderFormData();
    // checkoutItemFormData();

    reloadTable();
    reloadActiveOrdersTable();

    // reloadTimeout();
    countdown();


    copyOrderUrl();
    hideSendMailBtn();

});//end of doc ready

function copyOrderUrl() {
    var inputUrl = $('#orderUrl');
    inputUrl.attr('title', 'click to copy url')
    inputUrl.attr('value', location.href);
    console.log(inputUrl.val() + "dsads" + location.href);


    $('#orderUrl').on('click', function () {
        $(this).focus();
        $(this).select();
        document.execCommand('copy');
    });

}

function test() {
    setInterval(function () {
        var url = location.pathname;
        // var orderId = url.split('/').pop();
        var split = url.split('/');
        var restaurantId = split[2];
        var orderId = split[4];
        if (url == "/restaurant/" + restaurantId + "/addItems/" + orderId) {


            $('#itList').html("/restaurant/" + restaurantId + "/addItems/" + orderId);
            console.log("/restaurant/" + restaurantId + "/addItems/" + orderId);
        }
    }, 4000);  //Delay here = 5 seconds
}

//reload items
function sendRequest(restaurantId, orderId) {
    // console.log("/restaurant/" + restaurantId + "/addItems/" + orderId);

    $.ajax({

        url: "/restaurant/" + restaurantId + "/addItems/" + orderId + '/list',
        method: 'GET',
        success:
            function (result) {
                $('#itList').html(result);

                setTimeout(function () {
                    sendRequest(restaurantId, orderId);
                    console.log(result + 'result');
                    console.log('sccess');
                    console.log("/restaurant/" + restaurantId + "/addItems/" + orderId);
                    $('#itList div h1 ').toggleClass('text-primary', 'text-success');
                }, 5000);
            },
        fail:
            function () {
                console.log("fail");
            }
    });
}

function reloadTable() {
    var url = location.pathname;
    // var orderId = url.split('/').pop();
    var split = url.split('/');
    var restaurantId = split[2];
    var orderId = split[4];
    console.log(restaurantId, orderId);

    if (url == "/restaurant/" + restaurantId + "/addItems/" + orderId) {
        {
            console.log(restaurantId + 'restaurant');
            console.log(orderId + 'order');
            console.log(url.split('/') + 'split');

            sendRequest(restaurantId, orderId);
        }
    }
}

// reload active orders
function getRequest(restaurantId) {
    console.log(location)
    $.ajax({
        url: "/restaurant/" + restaurantId + "/activeOrders/list",
        method: 'GET',
        success:
            function (result) {
                $('#activeOrder').html(result);
                setTimeout(function () {
                    getRequest(restaurantId);
                    console.log(result + 'result');
                    console.log('sccess');

                    // $('#itList div h1 ').toggleClass('text-primary', 'text-success');
                }, 5000);
            },
        fail:
            function () {
                console.log("fail");
            }
    });
}

function reloadActiveOrdersTable() {
    var url = location.pathname;
    var split = url.split('/');
    var restaurantId = split[2];
    console.log(restaurantId);

    if (url == "/restaurant/" + restaurantId + "/activeOrders") {
        {
            getRequest(restaurantId);
        }
    }
}

function reloadTimeout() {
    var url = location.pathname;
    var split = url.split('/');
    var restaurantId = split[2];
    var orderId = split[4];

    if (url == "/restaurant/" + restaurantId + "/addItems/" + orderId) {

        console.log(restaurantId + "ids" + orderId);
        console.log(url);
        getTimer(restaurantId, orderId);

    }
}

function countdown() {
    var timeLeft = $("#timerVal").val();
    var form = $('#newItemForm');
    var cdown = setInterval(function () {
        // console.log(timeLeft + ' timer val');
        var hours = Math.floor((timeLeft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((timeLeft % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((timeLeft % (1000 * 60)) / 1000);
        timeLeft -= 1000;
        $("#countdown").html(hours + "h "
            + minutes + "m " + seconds + "s ");

        // if (timeLeft < 0) {
        //     clearInterval(cdown);
        //     $('#countdown').html('EXPIRED').css('color','red');
        //     $('#newItemContainer').html('<h1>Order has been expired</h1>').css('color','red');
        // }
        if (timeLeft <= 0) {
            $('#countdown').html('EXPIRED').css('color', 'red');
            $('#newItemContainer').html('<h1>Order has been expired</h1>').css('color', 'red');
            $('#orderUrl').attr('disabled', 'disabled').css('visibility', 'hidden');
            $('#mail-not-sent').css('visibility', 'visible');

            $('#order-sent').css('visibility','visible');
            clearInterval(cdown);
        }
        if (timeLeft <= 1100 && timeLeft >= 0) {
            $('#send-email').css('visibility', 'visible');
        }

    }, 1000);
    if (timeLeft <= 0) {
        addNewItemFormDisabled();
        $('#mail-not-sent').css('visibility', 'visible');
        $('#order-sent').css('visibility','visible');
        // $('#send-email').css('visibility','visible');
    }
}

function hideSendMailBtn() {
    $('#send-email').on('click', function f() {
        // $('#send-email').css('visibility', 'hidden');
        $('#send-email').fadeOut(1000);
    });
}


// function getTimer(restaurantId, orderId) {
//     console.log(location + 'timeout ajax')
//     $.ajax({
//         url: "/restaurant/" + restaurantId + "/addItems/" + orderId + "/timer",
//         method: 'GET',
//         dataType: "json",
//         // data: data,
//         success:
//             function (data) {
//                 $("#timeout").html(data);
//                 setTimeout(function () {
//                     getTimer(restaurantId,orderId);
//                     console.log(data + 'result');
//                     console.log('sccess');
//
//                     // $('#itList div h1 ').toggleClass('text-primary', 'text-success');
//                 }, 1000);
//             },
//         fail:
//             function () {
//                 console.log("fail");
//             }
//     });
// }
// /////////////////////
//
// /////////////////////
//
// function reloadTimeout() {
//     var url = location.pathname;
//     var split = url.split('/');
//     var restaurantId = split[2];
//     var orderId = split[4];
//
//     if (url == "/restaurant/" + restaurantId + "/addItems/" + orderId) {
//         {
//     console.log(restaurantId+ "ids" +orderId);
//     console.log(url);
//             getTimer(restaurantId, orderId);
//         }
//     }
// }


// restaurant details disable inputs
function disableFormElements() {
    /*<![CDATA[*/
    var path = /*[[@{/}]]*/'/restaurant/restaurantDetails/';
    /*]]>*/
    var url = location.pathname;
    var id = url.substring(url.lastIndexOf('/') + 1);
    console.log(id);
    console.log("path " + path + id);
    console.log("loc " + location.pathname);

    if (location.pathname === path + id) {
        $("#restaurantForm :input").prop('readonly', true);
        $("#restaurantForm select").prop('disabled', true);
        $("#restaurantForm button").css('display', 'none !important');
        $("#restaurantForm input[type=\"file\"]").css('display', 'none');
    }
}

//Front End Validation
//##################################################################################

function addNewItemFormDisabled() {
    var form = $('#newItemForm');
    form.submit(function (event) {
        event.preventDefault();
    });

}


function validateCreatorNameField(name, event) {
    if (!isValidName(name)) {
        $('#creator-error-info').text('Name should be longer than 2 characters').fadeOut(2500);
        $('#creator-error-info').css('border-bottom', '1px solid red');
        event.preventDefault();
    } else {
        $('#creator-error-info').text('');
        $('#creator-error-info').css('border-bottom', '1px solid lime');
    }
}

function validateItemNameField(name, event) {
    if (!isValidName(name)) {
        $('#item-error-info').text("Can't be shorter than 2 characters").fadeOut(2500);
        $('#item-error-info').css('border-bottom', '1px solid red');
        event.preventDefault();
    } else {
        $('#item-error-info').text('');
        $('#item-error-info').css('border-bottom', '1px solid lime');
    }
}

function validatePriceField(value, event) {

    if (!isValidPrice(value)) {
        $('#price-error-info').text('Not a valid price').fadeOut(2500);
        $('#price-error-info').css('border-bottom', '1px solid red');
        event.preventDefault();
    } else {
        $('#price-error-info').text('');
        $('#price-error-info').css('border-bottom', '1px solid lime');
    }
}

function isValidPrice(value) {
    var floatRegex = '[+]?([0-9]*.[0-9]+|[0-9]+)';
    var validPriceRegex = '^\\d+(,\\d{3})*(\\.\\d{1,2})?$';
    return value.match(validPriceRegex);
}

function isValidName(name) {
    var nameRegex = '^[a-zA-Z]+(([\',. -][a-zA-Z ])?[a-zA-Z]*)*$';
    return name.match(nameRegex);
}

function checkoutItemFormData() {
    var form = $('#newItemForm');
    enableFastFeedbackItemForm(form);

    form.submit(function (event) {
        var name = $('#itemCreator').val();
        var item = $('#itemName').val();
        var price = $('#itemPrice').val();
        console.log(name, item, price);
        validateCreatorNameField(name, event);
        validateItemNameField(item, event);
        validatePriceField(price, event);
    });
}

function checkOrderFormData() {

    var form = $('#newOrderForm');
    enableFastFeedback(form);

    form.submit(function (event) {
        var name = $('#creator').val();
        validateCreatorNameField(name, event);

    });


};

function enableFastFeedbackItemForm(form) {
    var nameInput = form.find('#itemCreator');
    var itemInput = form.find('#itemName');
    var priceInput = form.find('#itemPrice');

    nameInput.blur(function (event) {
        var name = $(this).val();
        validateCreatorNameField(name, event);
        if (!isValidName(name)) {
            nameInput.css('border-bottom', '1px solid red');
        } else {
            nameInput.css('border-bottom', '1px solid lime');
        }
    });
    itemInput.blur(function (event) {
        var item = $(this).val();
        validateItemNameField(item, event);
        if (!isValidName(item)) {
            itemInput.css('border-bottom', '1px solid red');
        } else {
            itemInput.css('border-bottom', '1px solid lime');
        }
    });
    priceInput.blur(function (event) {
        var price = $(this).val();
        validatePriceField(price, event);
        if (!isValidPrice(price)) {
            priceInput.css('border-bottom', '1px solid red');
        } else {
            priceInput.css('border-bottom', '1px solid lime');
        }
    })

}

function enableFastFeedback(formElement) {
    var nameInput = formElement.find('#creator');

    nameInput.blur(function (event) {
        var name = $(this).val();
        console.log(name);
        validateCreatorNameField(name, event);
        if (!isValidName(name)) {
            nameInput.css('border-bottom', '1px solid red');
        } else {
            nameInput.css('border-bottom', '1px solid lime');
        }
    });
}
