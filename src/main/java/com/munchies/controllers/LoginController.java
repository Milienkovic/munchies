package com.munchies.controllers;

import com.munchies.models.dtos.UserDTO;
import com.munchies.models.entities.User;
import com.munchies.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @RequestMapping("/login")
    public ModelAndView login() {
        ModelAndView model = new ModelAndView();
        model.setViewName("login2");
        return model;
    }

    @RequestMapping("/test")
    public ModelAndView test() {
        ModelAndView model = new ModelAndView();
        model.setViewName("login1");
        return model;
    }

    @RequestMapping("/registration")
    public ModelAndView registration() {
        ModelAndView model = new ModelAndView();
        UserDTO user = new UserDTO();
        model.addObject("user", user);
        model.setViewName("registrationForm2");
        return model;
    }

    @PostMapping("/registration")
    public ModelAndView createNewUser(@Valid @ModelAttribute("user") UserDTO user, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();

        UserDTO userExists = userService.findByEmail(user.getEmail());
        if (userExists != null) {
            bindingResult
                    .rejectValue("email", "error.user",
                            "There is already a user registered with the email provided");
        }
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("registrationForm2");

        } else {
            userService.saveUser(user);
            modelAndView.addObject("successMessage", "User has been registered successfully");
//            modelAndView.addObject("user", new UserDTO());
            modelAndView.setViewName("redirect:/login");
        }

//        userService.saveUser(user);
//            modelAndView.addObject("successMessage", "User has been registered successfully");
//            modelAndView.addObject("user", new User());
//        modelAndView.setViewName("redirect:/login");

        return modelAndView;
    }

}
