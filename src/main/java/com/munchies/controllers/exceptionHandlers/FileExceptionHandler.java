package com.munchies.controllers.exceptionHandlers;

import com.munchies.services.exceptions.FileSizeException;
import com.munchies.services.exceptions.StorageException;
import com.munchies.services.exceptions.StorageFileNotFoundException;
import com.munchies.util.StorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@ControllerAdvice
public class FileExceptionHandler {

    @Autowired
    private StorageProperties storageProperties;

    @ExceptionHandler(FileSizeException.class)
    public String fileToLarge(FileSizeException e, RedirectAttributes redirectAttributes){
//        ModelAndView model = new ModelAndView();
//        model.addObject("message","File is Larger than "+storageProperties.getMaxFileSize());
//        model.setViewName("uploadStatus");
        redirectAttributes.addAttribute("message","File is Larger than "+storageProperties.getMaxFileSize());
        return "redirect:/uploadStatus";
    }

    @ExceptionHandler(StorageException.class)
    public String storageException(StorageException e,RedirectAttributes redirectAttributes){
//        ModelAndView model = new ModelAndView();
//        model.addObject("message","Storage not Found");
//        model.setViewName("uploadStatus");
        redirectAttributes.addAttribute("message","Storage not Found");
        return "redirect:/uploadStatus";
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public String fileNotFound(StorageFileNotFoundException e,RedirectAttributes redirectAttributes){
//        ModelAndView model = new ModelAndView();
//        model.addObject("message","File not Found");
//        model.setViewName("uploadStatus");
        redirectAttributes.addAttribute("message","File not Found");
        return "redirect:/uploadStatus";
    }

}
