package com.munchies.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ExceptionController {
    @RequestMapping("/uploadStatus")
    public ModelAndView uploadStatus() {
        return new ModelAndView("uploadStatus");
    }
}
