package com.munchies.controllers;


import com.munchies.services.impl.mail.runnable.MyTask;
import com.munchies.util.StorageProperties;
import com.munchies.models.dtos.RestaurantDTO;
import com.munchies.models.dtos.UserDTO;
import com.munchies.models.entities.Restaurant;
import com.munchies.services.RestaurantService;
import com.munchies.services.StorageService;
import com.munchies.services.UserService;
import com.munchies.util.GeneralUtil;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.io.*;
import java.util.List;

@Controller
@RequestMapping("/restaurant")
public class RestaurantController {

    @Autowired
    private UserService userService;

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private StorageService storageService;

    @Autowired
    private StorageProperties storageProperties;

    @GetMapping("/home")
    public ModelAndView home() {
        ModelAndView model = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDTO user = userService.findByEmail(auth.getName());
        List<RestaurantDTO> restaurantList = restaurantService.findAll();

        model.addObject("restaurantList", restaurantList);
        model.setViewName("restaurantHome");

        return model;
    }

    @GetMapping("/admin/addRestaurant")
    public ModelAndView addRestaurant() {
        ModelAndView model = new ModelAndView();
        Restaurant restaurant = new Restaurant();
        model.addObject("restaurant", restaurant);
        model.setViewName("addRestaurant");
        model.addObject("addName", "Add New Restaurant");
        model.addObject("addNew", "Add New Restaurant");
        List<Integer> deliveryTimeList = restaurantService.getDeliveryTimeList();

        model.addObject("deliveryTimeList", deliveryTimeList);
        return model;
    }

    @PostMapping("/admin/saveRestaurant")
    public ModelAndView saveRestaurant(@Valid @ModelAttribute("restaurant") RestaurantDTO restaurantDTO, BindingResult bindingResult,
                                       @RequestParam("file") MultipartFile file) {
        ModelAndView model = new ModelAndView();
        RestaurantDTO restaurantCheckByPhone = restaurantService.findByPhone(restaurantDTO.getPhone());
        RestaurantDTO restraurantCheckByName = restaurantService.findByName(restaurantDTO.getName());
        RestaurantDTO restaurantCheckByEmail = restaurantService.findByEmail(restaurantDTO.getEmail());

        model.addObject("addName", "Add New Restaurant");
        if ((restaurantCheckByPhone != null && restaurantDTO.getId() == 0)
                || (restaurantCheckByPhone != null && restaurantCheckByPhone.getId() != restaurantDTO.getId())) {
            bindingResult
                    .rejectValue("phone", "error.restaurant",
                            "There is already a restaurant registered with the phone provided");
            model.addObject("addName", "Edit Restaurant");
        }
        if ((restraurantCheckByName != null && restaurantDTO.getId() == 0) || (restraurantCheckByName != null && restaurantDTO.getId() != restraurantCheckByName.getId())) {
            bindingResult.rejectValue("name", "error.restaurant", "There is already a restaurant registered with that name");
            model.addObject("addName", "Edit Restaurant");
        }

        if ((restaurantCheckByEmail != null && restaurantDTO.getId() == 0) || (restaurantCheckByEmail != null && restaurantDTO.getId() != restaurantCheckByEmail.getId())) {
            bindingResult.rejectValue("email", "error.restaurant", "There is already a restaurant registered with that email");
            model.addObject("addName", "Edit Restaurant");
        }

        if (file.isEmpty() && restaurantDTO.getId() == 0) {
//            model.addObject("message", "Please select a file to upload");
            bindingResult.rejectValue("menuUrl", "error.restaurant", "Please select a file to upload");
        }


        if (bindingResult.hasErrors()) {
            List<Integer> deliveryTimeList = GeneralUtil.timeList();
            model.addObject("deliveryTimeList", deliveryTimeList);
            model.setViewName("addRestaurant");
        } else {
            try {
                model.addObject("addName", "Add New Restaurant");
                model.addObject("successMessage", "Restaurant has been added");
               restaurantService.saveOrUpdate(restaurantDTO,file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            model.setViewName("redirect:/restaurant/home");
        }
        return model;
    }

    @GetMapping("/restaurantDetails/{id}/admin/deleteRestaurant/")
    public ModelAndView deleteRestaurant(@PathVariable("id") Integer id) {
        restaurantService.deleteById(id);
        return new ModelAndView("redirect:/restaurant/home");
    }

    @GetMapping("/admin/editRestaurant/{id}")
    public ModelAndView editRestaurant(@PathVariable Integer id) {
        ModelAndView model = new ModelAndView();
        com.munchies.models.dtos.RestaurantDTO restaurant = restaurantService.findById(id);
        List<Integer> deliveryTimeList = restaurantService.getDeliveryTimeList();
        model.addObject("restaurant", restaurant);
        model.addObject("deliveryTimeList", deliveryTimeList);
        model.addObject("addName", "Edit Restaurant");
        model.addObject("editRest", "Edit Restaurant Details");
        model.setViewName("addRestaurant");
        return model;
    }

    @GetMapping("/restaurantDetails/{id}")
    public ModelAndView restaurantDetails(@PathVariable int id) {
        ModelAndView model = new ModelAndView();
        com.munchies.models.dtos.RestaurantDTO restaurant = restaurantService.findById(id);
        List<Integer> deliveryTimeList = restaurantService.getDeliveryTimeList();
        model.addObject("deliveryTimeList", deliveryTimeList);
        model.addObject("restaurant", restaurant);
        model.addObject("restaurantDetails", true);
        model.addObject("hideBtn", true);
        model.setViewName("addRestaurant");
        return model;
    }
}
