package com.munchies.controllers;

import com.munchies.models.dtos.GroupOrderDTO;
import com.munchies.models.dtos.ItemDTO;
import com.munchies.models.dtos.RestaurantDTO;
import com.munchies.services.*;
import com.munchies.services.impl.mail.MailService;
import com.munchies.util.GeneralUtil;
import com.munchies.util.StorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.*;

@Controller
public class GroupOrderController {

    @Autowired
    private GroupOrderService groupOrderService;

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private ItemService itemService;

    @Autowired
    private StorageService storageService;

    @Autowired
    MailService mailService;

    @GetMapping("restaurant/{restaurantid}/newGroupOrder")
    public ModelAndView newGroupOrder(@PathVariable("restaurantid") int restaurantid) {
        RestaurantDTO restaurant = restaurantService.findById(restaurantid);

        ModelAndView model = new ModelAndView();
        System.out.println(restaurant);
        List<Integer> timeouts = GeneralUtil.timeList();
        model.addObject("groupOrder", new GroupOrderDTO());
        model.addObject("restaurant", restaurant);
        model.addObject("timeouts", timeouts);
        model.setViewName("createNewOrder");
        return model;
    }

    @PostMapping("/restaurant/{restaurantid}/createOrder")
    public ModelAndView createOrder(@PathVariable int restaurantid, @Valid @ModelAttribute("order") GroupOrderDTO order, BindingResult bindingResult) {
        ModelAndView model = new ModelAndView();
        RestaurantDTO restaurant = restaurantService.findById(restaurantid);

        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult.getFieldErrors().get(0));
            List<Integer> timeouts = GeneralUtil.timeList();
            model.addObject("groupOrder", new GroupOrderDTO());
            model.addObject("restaurant", restaurant);
            model.addObject("timeouts", timeouts);
            model.setViewName("createNewOrder");
        } else {
            GroupOrderDTO groupOrder = groupOrderService.saveOrUpdate(order,restaurant);
            if (groupOrder.isAutoSend() && !groupOrder.isSent()) {
                mailService.sendAutoMail(groupOrder.getId());
            }
            model.setViewName("redirect:/restaurant/" + restaurant.getId() + "/addItems/" + groupOrder.getId());
        }

        return model;
    }

    @GetMapping("/restaurant/{restaurantid}/addItems/{orderId}")
    public ModelAndView addItem(@PathVariable int restaurantid, @PathVariable int orderId) {
        ModelAndView model = new ModelAndView();
        RestaurantDTO restaurant = restaurantService.findById(restaurantid);
        GroupOrderDTO groupOrder = groupOrderService.findById(orderId);

        String fileName = restaurant.getMenuUrl();
        model.addObject("fileName", fileName);
        model.addObject("timer", groupOrderService.timer(orderId));
        model.addObject("restaurant", restaurant);
        model.addObject("order", groupOrder);
        model.addObject("item", new ItemDTO());
        model.setViewName("addOrderItems");


        return model;
    }

    @PostMapping("/restaurant/{restaurantid}/addItems/{orderId}")
    public ModelAndView addItem(@PathVariable int restaurantid, @PathVariable int orderId, @Valid @ModelAttribute("item") ItemDTO item, BindingResult bindingResult) {
        ModelAndView model = new ModelAndView();
        RestaurantDTO restaurant = restaurantService.findById(restaurantid);
        GroupOrderDTO groupOrder = groupOrderService.findById(orderId);
        if (bindingResult.hasErrors()) {
            model.addObject("timer", groupOrderService.timer(orderId));
            model.addObject("restaurant", restaurant);
            model.addObject("order", groupOrder);
//            model.addObject("item", new ItemDTO());
            model.setViewName("addOrderItems");
        } else {
            //todo refactor done
//            groupOrder.addItem(item);
            ItemDTO itemDTO = itemService.saveOrUpdate(item,groupOrder);
            model.addObject("orderCreated", true);
            model.addObject("restaurant", restaurant);
            model.addObject("order", groupOrder);
            model.addObject("item", itemDTO);
            RedirectView redirectView = new RedirectView("/restaurant/{restaurantid}/addItems/{orderId}");
            redirectView.setExposeModelAttributes(false);
            model.setView(redirectView);
        }
        return model;
    }

    @GetMapping("/restaurant/{restaurantid}/addItems/{orderId}/list")
    public ModelAndView itemList(@PathVariable int restaurantid, @PathVariable int orderId) {
        ModelAndView model = new ModelAndView();
        GroupOrderDTO groupOrder = groupOrderService.findOne(orderId);
        model.addObject("order", groupOrder);
        model.setViewName("listOfItems :: listOfItems");
        return model;
    }

    @RequestMapping("/restaurant/{restaurantId}/activeOrders")
    public ModelAndView listOfActiveOrders(@PathVariable int restaurantId) {
        ModelAndView model = new ModelAndView();
        RestaurantDTO restaurant = restaurantService.findOne(restaurantId);
        List<GroupOrderDTO> activeOrders = groupOrderService.activeOrders(restaurantId);
        model.addObject("restaurant", restaurant);
        model.addObject("orders", activeOrders);
        model.setViewName("listOfGroupOrders");
        return model;
    }

    @GetMapping("/restaurant/{restaurantId}/activeOrders/list")
    public ModelAndView activeOrders(@PathVariable int restaurantId, ModelAndView model) {
//        ModelAndView models = new ModelAndView();
        RestaurantDTO restaurant = restaurantService.findOne(restaurantId);
        List<GroupOrderDTO> activeOrders = groupOrderService.activeOrders(restaurantId);
        model.addObject("restaurant", restaurant);
        model.addObject("orders", activeOrders);
        model.setViewName("activeOrders :: activeOrders");
        return model;
    }

    @GetMapping("/restaurant/{restaurantId}/addItems/{orderId}/timer")
    public @ResponseBody
    Long orderTimer(@PathVariable int restaurantId, @PathVariable int orderId) {
        GroupOrderDTO groupOrderDTO = groupOrderService.findById(orderId);
        return groupOrderService.timer(orderId);
    }

    @RequestMapping("/download/{fileName}")
    public void downloadPDFResource(HttpServletResponse response,
                                    @PathVariable("fileName") String fileName) {
        try {
            response.setContentType("application/octet-stream");
            response.addHeader("Content-Disposition","attachment; fileName="+fileName);
            storageService.download(response.getOutputStream(),fileName);

        } catch (IOException e) {
            e.printStackTrace();
        }
      }

    @RequestMapping("/restaurant/{restaurantId}/addItems/{orderId}/send")
    public ModelAndView sendEmail(@PathVariable int restaurantId, @PathVariable int orderId) {
        ModelAndView model = new ModelAndView();
        GroupOrderDTO order = groupOrderService.findById(orderId);
        MimeMessagePreparator mimeMessagePreparator = mailService.constructOrderEmail(order);
        mailService.sendMail(mimeMessagePreparator, orderId);
        model.setViewName("redirect:/restaurant/" + restaurantId + "/addItems/" + orderId);
        return model;
    }
}
