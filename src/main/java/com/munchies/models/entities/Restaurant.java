package com.munchies.models.entities;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Restaurant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "phone")
    private String phone;

    @Column(name = "menu_url")
    private String menuUrl;

    @Column(name = "delivery_info")
    private String deliveryInfo;

    @Column(name = "additional_charges")
    private String additionalCharges;

    @Column(name = "email")
    private String email;



    @OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<GroupOrder> groupOrders;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }


    public String getDeliveryInfo() {
        return deliveryInfo;
    }

    public void setDeliveryInfo(String deliveryInfo) {
        this.deliveryInfo = deliveryInfo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String emai) {
        this.email = emai;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Restaurant that = (Restaurant) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(address, that.address) &&
                Objects.equals(phone, that.phone) &&
                Objects.equals(menuUrl, that.menuUrl) &&
                Objects.equals(deliveryInfo, that.deliveryInfo)&&
                Objects.equals(additionalCharges, that.additionalCharges);
    }
    public String getAdditionalCharges() {
        return additionalCharges;
    }

    public void setAdditionalCharges(String additionalCharges) {
        this.additionalCharges = additionalCharges;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, address, phone, menuUrl, deliveryInfo);
    }

    public List<GroupOrder> getGroupOrders() {
        return groupOrders;
    }

    public void setGroupOrders(List<GroupOrder> groupOrdersById) {
        this.groupOrders = groupOrdersById;
    }



//    public void addGroupOrder(GroupOrder order){
//        if(groupOrders==null){
//            groupOrders= new ArrayList<>();
//        }
//        order.setRestaurant(this);
//        groupOrders.add(order);
//    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", deliveryInfo='" + deliveryInfo + '\'' +
                ", additionalCharges='" + additionalCharges + '\'' +
                ", groupOrdersById=" + groupOrders +
                '}';
    }
}
