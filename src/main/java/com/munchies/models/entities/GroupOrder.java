package com.munchies.models.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "group_order", schema = "munchies")
public class GroupOrder implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "timeout")
    private String timeout;

    @Column(name = "creator")
    private String creator;

    @ManyToOne
    @JoinColumn(name = "restaurant_id")
    private Restaurant restaurant;

    @Column(name = "tstamp")
    private Timestamp timestamp;

    @Column(name = "auto_send")
    private boolean autoSend;
    @Column(name = "sent")
    private boolean sent;

    @OneToMany(mappedBy = "groupOrder", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Item> items;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }


    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupOrder that = (GroupOrder) o;
        return id == that.id &&
                Objects.equals(timeout, that.timeout) &&
                Objects.equals(creator, that.creator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, timeout, creator);
    }


    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurantByRestaurantId) {
        this.restaurant = restaurantByRestaurantId;
    }


    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> itemsById) {
        this.items = itemsById;
    }

//    public void addItem(Item item) {
//        if (items == null) {
//            items = new ArrayList<>();
//        }
//        item.setGroupOrder(this);
//        items.add(item);
//    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isAutoSend() {
        return autoSend;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public void setAutoSend(boolean autoSend) {
        this.autoSend = autoSend;
    }

    @Override
    public String toString() {
        return "GroupOrder{" +
                id + " " +
                timeout + ' ' +
                creator + ' ' +
//                ", items=" + items +
                '}';
    }
}
