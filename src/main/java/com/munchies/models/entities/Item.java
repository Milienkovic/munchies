package com.munchies.models.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "item_name")
    private String itemName;

    @Column(name = "creator")
    private String creator;

    @Column(name = "price")
    private double price;

    @ManyToOne
    @JoinColumn(name = "group_order_id", referencedColumnName = "id")
    private GroupOrder groupOrder;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return id == item.id &&
                Double.compare(item.price, price) == 0 &&
                Objects.equals(itemName, item.itemName) &&
                Objects.equals(creator, item.creator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, itemName, creator, price);
    }

    public GroupOrder getGroupOrder() {
        return groupOrder;
    }

    public void setGroupOrder(GroupOrder groupOrderByGroupOrderId) {
        this.groupOrder = groupOrderByGroupOrderId;
    }


}
