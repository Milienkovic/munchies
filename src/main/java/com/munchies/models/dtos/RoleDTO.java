package com.munchies.models.dtos;

import java.util.ArrayList;
import java.util.List;

public class RoleDTO {

    private int id;
    private String role;
    private List<UserDTO> users;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UserDTO> users) {
        this.users = users;
    }

    public void addUser(UserDTO userDTO){
        if (users==null){
            users = new ArrayList<>();
        }
        users.add(userDTO);
    }
}
