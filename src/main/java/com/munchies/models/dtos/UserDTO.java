package com.munchies.models.dtos;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

public class UserDTO {

    private int id;

    @Pattern(regexp = "^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$",message ="Invalid First Name" )
    @Size(max = 25, min = 2)
    private String firstName;

    @Pattern(regexp = "^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$",message = "Invalid Last Name")
    @Size(max = 25, min = 2)
    private String lastName;

    @Pattern(regexp = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$", message = "Invalid Phone Number")
    private String phone;

    @Pattern(regexp = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", message = "Invalid E-mail")
    private String email;

    @Pattern(regexp = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$", message = "Incorrect Password, must contain at least 1 Upper case and Digit")
    @Size(min = 4, max = 8, message = " between  4 and 8 characters")
    private String password;

    @Size(max = 50, min = 2, message = "Invalid Address")
    private String address;

    private Set<RoleDTO> userRoles;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

      public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<RoleDTO> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<RoleDTO> userRoles) {
        this.userRoles = userRoles;
    }

    public void addUserRole(RoleDTO roleDTO){
        if (userRoles==null){
            userRoles = new HashSet<>();
        }
        userRoles.add(roleDTO);
    }
}
