package com.munchies.models.dtos;

import com.munchies.models.entities.Restaurant;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class GroupOrderDTO {

    private int id;

//    @NumberFormat(pattern ="^[1-9]\\d*$")
    private String timeout;

    @Size(min = 2, max = 25, message = "Invalid Name 2-25 characters")
    private String creator;

    private RestaurantDTO restaurant;

    private Timestamp timestamp;

    @NotNull
    private boolean autoSend;

    @NotNull
    private boolean sent;

    private List<ItemDTO> items;

    public List<ItemDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemDTO> items) {
        this.items = items;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public RestaurantDTO getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(RestaurantDTO restaurant) {
        this.restaurant = restaurant;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isAutoSend() {
        return autoSend;
    }

    public void setAutoSend(boolean autoSend) {
        this.autoSend = autoSend;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public void addItem(ItemDTO itemDTO) {
        if (items == null) {
            items = new ArrayList<>();
        }
        items.add(itemDTO);
    }

//    @Override
//    public String toString() {
//        return "GroupOrderDTO{" +
//                "id=" + id +
//                ", timeout='" + timeout + '\'' +
//                ", creator='" + creator + '\'' +
//                ", restaurant=" + restaurant +
//                ", timestamp=" + timestamp +
//                ", autoSend=" + autoSend +
//                ", sent=" + sent +
//                ", items=" + items +
//                '}';
//    }
}
