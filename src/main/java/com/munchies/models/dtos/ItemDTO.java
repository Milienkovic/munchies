package com.munchies.models.dtos;




import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class ItemDTO {
    private int id;

    @Size(min = 2,max = 25, message = "Invalid Item Name")
    private String itemName;

    @Size(min = 2,max = 25, message = "Invalid Creator Name")
    private String creator;

//    @Pattern(regexp = "(\\d{1,4}\\.\\d{0,2})", message = "Invalid Price")
    @NumberFormat(pattern = "###.##")
    private double price;

    private GroupOrderDTO groupOrder;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public GroupOrderDTO getGroupOrder() {
        return groupOrder;
    }

    public void setGroupOrder(GroupOrderDTO groupOrder) {
        this.groupOrder = groupOrder;
    }
}
