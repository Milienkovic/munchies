package com.munchies.models.dtos;

import org.springframework.web.bind.annotation.Mapping;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class RestaurantDTO {

    private int id;

    @Size(min = 2, max = 30)
    private String name;

    @Size(min = 2,max = 30)
    private String address;

    @Pattern(regexp = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$", message = "Invalid Phone Number")
    private String phone;

    @Size(min = 2, max = 250)
//    @NotNull
    private String menuUrl;

//    @Size(min = 2, max = 250)
    private String deliveryInfo;

    @Size(min = 2, max = 250)
    private String additionalCharges;

    @Pattern(regexp = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", message = "Invalid E-mail")
    private String email;

    private List<GroupOrderDTO> groupOrders;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getDeliveryInfo() {
        return deliveryInfo;
    }

    public void setDeliveryInfo(String deliveryInfo) {
        this.deliveryInfo = deliveryInfo;
    }

    public String getAdditionalCharges() {
        return additionalCharges;
    }

    public void setAdditionalCharges(String additionalCharges) {
        this.additionalCharges = additionalCharges;
    }

    public List<GroupOrderDTO> getGroupOrders() {
        return groupOrders;
    }

    public void setGroupOrders(List<GroupOrderDTO> groupOrders) {
        this.groupOrders = groupOrders;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void addGroupOrder(GroupOrderDTO order) {
        if (groupOrders==null){
            groupOrders= new ArrayList<>();
        }
        groupOrders.add(order);
    }

    public String getShortName(){
        return name.replaceAll(" ","_");
    }

//    @Override
//    public String toString() {
//        return "RestaurantDTO{" +
//                "id=" + id +
//                ", name='" + name + '\'' +
//                ", address='" + address + '\'' +
//                ", phone='" + phone + '\'' +
//                ", menuUrl='" + menuUrl + '\'' +
//                ", deliveryInfo='" + deliveryInfo + '\'' +
//                ", additionalCharges='" + additionalCharges + '\'' +
//                ", email='" + email + '\'' +
//                ", groupOrders=" + groupOrders +
//                '}';
//    }
}
