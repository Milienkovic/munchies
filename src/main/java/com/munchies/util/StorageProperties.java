package com.munchies.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
public class StorageProperties {
    @Value("${file.upload-dir}")
//    private String location = "C:\\Users\\mix\\IdeaProjects\\munchies\\src\\main\\resources\\savedFiles";
    private String location;

    @Value("${spring.servlet.multipart.max-file-size}")
    private String maxFileSizeString;

    private Long maxFileSize=0l;

    public Long getMaxFileSize() {
        if (maxFileSizeString.substring(maxFileSizeString.length() - 2, maxFileSizeString.length() - 1).equalsIgnoreCase("KB")) {
            maxFileSizeString = maxFileSizeString.replaceAll("\\D", "");
            maxFileSize = Long.parseLong(maxFileSizeString) * 1024;
        } else if (maxFileSizeString.substring(maxFileSizeString.length() - 2, maxFileSizeString.length()).equalsIgnoreCase("MB")) {
            maxFileSizeString = maxFileSizeString.replaceAll("\\D", "");
            maxFileSize = Long.parseLong(maxFileSizeString) * 1024 * 1024;
        } else if (maxFileSizeString.substring(maxFileSizeString.length() - 2, maxFileSizeString.length()).equalsIgnoreCase("GB")) {
            maxFileSizeString = maxFileSizeString.replaceAll("\\D", "");
            maxFileSize = Long.parseLong(maxFileSizeString) * 1024 * 1024 * 1024;
            return maxFileSize;
        }
        return maxFileSize;
    }

    public void setMaxFileSize(Long maxFileSize) {
        this.maxFileSize = maxFileSize;
    }


    public String getLocation() {
        return location;

    }

    public void setLocation(String location) {
        this.location = location;
    }


}
