package com.munchies.util;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GeneralUtil {
    public static List<Integer> timeList() {
        List<Integer> list = new ArrayList<>();
        Integer[] time = {1,5, 10, 15, 20, 30, 45, 60};
        list.addAll(Arrays.asList(time));
        return list;

    }
}
