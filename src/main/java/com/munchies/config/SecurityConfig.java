package com.munchies.config;

import com.munchies.services.impl.security.UserSecurityService;
import com.munchies.util.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.security.SecureRandom;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserSecurityService userSecurityService;



    private static final String[] PUBLIC_MATCHERS = {
            "/css/**",
            "/js/**",
            "/image/**",
            "/login",
            "/registration",
            "/restaurant/**",
            "/addItems/**",
            "/test",
            "/activeOrders",
//            "/admin/**",
            "/uploadStatus/**",
            "/download/**"

    };

    private static final String[] ADMIN_MATCHERS = {
            "/admin/**"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers(PUBLIC_MATCHERS).permitAll().anyRequest().anonymous()
                .antMatchers(ADMIN_MATCHERS).hasAuthority("ADMIN").anyRequest().authenticated();
        http.formLogin()
                .failureUrl("/login?error").defaultSuccessUrl("/restaurant/home").loginPage("/login")
                .and()
                .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/restaurant/home")
                .and().rememberMe();
    }


    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userSecurityService).passwordEncoder(SecurityUtil.passwordEncoder());
    }

    private BCryptPasswordEncoder passwordEncoder() {
        return SecurityUtil.passwordEncoder();
    }
}
