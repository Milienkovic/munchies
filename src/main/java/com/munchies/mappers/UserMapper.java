package com.munchies.mappers;

import com.munchies.models.dtos.UserDTO;
import com.munchies.models.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserMapper {

    @Autowired
    private RoleMapper roleMapper;

    public UserDTO mapToDto(User user, boolean mapRoles) {
        if (user == null) {
            return null;
        }
        UserDTO userDTO = new UserDTO();
        userDTO.setAddress(user.getAddress());
        userDTO.setEmail(user.getEmail());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setId(user.getId());
        userDTO.setPassword(user.getPassword());
        userDTO.setPhone(user.getPhone());
        if (mapRoles) {
            userDTO.setUserRoles(roleMapper.mapToDtoList(user.getUserRoles()));
        }

        return userDTO;
    }

    public User mapToEntity(UserDTO userDTO, boolean mapRoles) {
        if (userDTO == null) {
            return null;
        }
        User user = new User();
        user.setAddress(userDTO.getAddress());
        user.setEmail(userDTO.getEmail());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setId(userDTO.getId());
        user.setPassword(userDTO.getPassword());
        user.setPhone(userDTO.getPhone());
        if (mapRoles) {
            user.setUserRoles(roleMapper.mapToEntityList(userDTO.getUserRoles()));
        }

        return user;
    }

    public List<UserDTO> mapToDtoList(List<User> users) {
        List<UserDTO> userDTOs = new ArrayList<>();
        for (User user :
                users) {
            userDTOs.add(mapToDto(user,true));
        }
        return userDTOs;
    }

    public List<User> mapToEntityList(List<UserDTO> userDTOs) {
        List<User> users = new ArrayList<>();
        for (UserDTO userDTO :
                userDTOs) {
            users.add(mapToEntity(userDTO,true));
        }
        return users;
    }
}
