package com.munchies.mappers;

import com.munchies.models.dtos.GroupOrderDTO;
import com.munchies.models.entities.GroupOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class GroupOrderMapper {

    @Autowired
    private ItemMapper itemMapper;

    @Autowired
    private RestaurantMapper restaurantMapper;

//    public GroupOrderDTO mapToDto(GroupOrder groupOrder) {
//        GroupOrderDTO groupOrderDTO = new GroupOrderDTO();
//        if (groupOrder == null) {
//            return null;
//        }
//            groupOrderDTO.setCreator(groupOrder.getCreator());
//            groupOrderDTO.setId(groupOrder.getId());
////            groupOrderDTO.setRestaurant(restaurantMapper.mapToDto(groupOrder.getRestaurant()));
//            groupOrderDTO.setTimeout(groupOrder.getTimeout());
//            groupOrderDTO.setTimestamp(groupOrder.getTimestamp());
////            groupOrderDTO.setItems(itemMapper.mapToDtoList(groupOrder.getItems()));
//
//        return groupOrderDTO;
//    }
//
//    public GroupOrder mapToEntity(GroupOrderDTO groupOrderDTO) {
//        GroupOrder groupOrder = new GroupOrder();
//        if (groupOrderDTO == null) {
//            return null;
//        }
////            groupOrder.setRestaurant(restaurantMapper.mapToEntity(groupOrderDTO.getRestaurant()));
//            groupOrder.setTimestamp(groupOrderDTO.getTimestamp());
//            groupOrder.setCreator(groupOrderDTO.getCreator());
//            groupOrder.setId(groupOrderDTO.getId());
//            groupOrder.setTimeout(groupOrderDTO.getTimeout());
////            groupOrder.setItems(itemMapper.mapToEntityList(groupOrderDTO.getItems()));
//
//        return groupOrder;
//    }

    public GroupOrderDTO mapToDto(GroupOrder groupOrder, boolean mapResttaurant, boolean mapItems) {
        if (groupOrder == null) {
            return null;
        }

        GroupOrderDTO groupOrderDTO = new GroupOrderDTO();

        groupOrderDTO.setCreator(groupOrder.getCreator());
        groupOrderDTO.setId(groupOrder.getId());
        if (mapResttaurant) {
            groupOrderDTO.setRestaurant(restaurantMapper.mapToDto(groupOrder.getRestaurant(),true));
        }
        groupOrderDTO.setTimeout(groupOrder.getTimeout());
        groupOrderDTO.setTimestamp(groupOrder.getTimestamp());
        if (mapItems) {
            groupOrderDTO.setItems(itemMapper.mapToDtoList(groupOrder.getItems()));
        }
        groupOrderDTO.setAutoSend(groupOrder.isAutoSend());
        groupOrderDTO.setSent(groupOrder.isSent());

        return groupOrderDTO;
    }

    public GroupOrder mapToEntity(GroupOrderDTO groupOrderDTO, boolean mapRestaurant, boolean mapItems) {
        GroupOrder groupOrder = new GroupOrder();
        if (groupOrderDTO == null) {
            return null;
        }
            if (mapRestaurant) {
                groupOrder.setRestaurant(restaurantMapper.mapToEntity(groupOrderDTO.getRestaurant(),true));
            }
            groupOrder.setTimestamp(groupOrderDTO.getTimestamp());
            groupOrder.setCreator(groupOrderDTO.getCreator());
            groupOrder.setId(groupOrderDTO.getId());
            groupOrder.setTimeout(groupOrderDTO.getTimeout());
            if (mapItems) {
                groupOrder.setItems(itemMapper.mapToEntityList(groupOrderDTO.getItems()));
            }
            groupOrder.setAutoSend(groupOrderDTO.isAutoSend());
            groupOrder.setSent(groupOrderDTO.isSent());
        return groupOrder;
    }

    public List<GroupOrderDTO> mapToDtoList(List<GroupOrder> groupOrders) {
        List<GroupOrderDTO> groupOrderDTOS = new ArrayList<>();
        for (GroupOrder groupOrder : groupOrders) {
            groupOrderDTOS.add(mapToDto(groupOrder,false,false));
        }
        return groupOrderDTOS;
    }

    public List<GroupOrder> mapToEntityList(List<GroupOrderDTO> groupOrderDTOS) {
        List<GroupOrder> groupOrders = new ArrayList<>();
        for (GroupOrderDTO groupOrderDTO :
                groupOrderDTOS) {
            groupOrders.add(mapToEntity(groupOrderDTO,false,false));
        }
        return groupOrders;
    }
}
