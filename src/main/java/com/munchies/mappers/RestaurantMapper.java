package com.munchies.mappers;


import com.munchies.models.dtos.RestaurantDTO;
import com.munchies.models.entities.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RestaurantMapper {

    @Autowired
    private GroupOrderMapper groupOrderMapper;

//    public RestaurantDTO mapToDto(Restaurant restaurant) {
//        RestaurantDTO restaurantDTO = new RestaurantDTO();
//        if (restaurant == null) {
//            return null;
//        }
//        restaurantDTO.setId(restaurant.getId());
//        restaurantDTO.setAdditionalCharges(restaurant.getAdditionalCharges());
//        restaurantDTO.setAddress(restaurant.getAddress());
//        restaurantDTO.setDeliveryInfo(restaurant.getDeliveryInfo());
////            restaurantDTO.setGroupOrders(groupOrderMapper.mapToDtoList(restaurant.getGroupOrders()));
//        restaurantDTO.setMenuUrl(restaurant.getMenuUrl());
//        restaurantDTO.setName(restaurant.getName());
//        restaurantDTO.setPhone(restaurant.getPhone());
//
//        return restaurantDTO;
//    }
//
//    public Restaurant mapToEntity(RestaurantDTO restaurantDTO) {
//        Restaurant restaurant = new Restaurant();
//        if (restaurantDTO == null) {
//            return null;
//        }
//        restaurant.setAdditionalCharges(restaurantDTO.getAdditionalCharges());
//        restaurant.setAddress(restaurantDTO.getAddress());
////            restaurant.setGroupOrders(groupOrderMapper.mapToEntityList(restaurantDTO.getGroupOrders()));
//        restaurant.setMenuUrl(restaurantDTO.getMenuUrl());
//        restaurant.setName(restaurantDTO.getName());
//        restaurant.setPhone(restaurantDTO.getPhone());
//        restaurant.setId(restaurantDTO.getId());
//        restaurant.setDeliveryInfo(restaurantDTO.getDeliveryInfo());
//
//        return restaurant;
//    }

    public RestaurantDTO mapToDto(Restaurant restaurant, boolean showGroupOrderList) {
        if (restaurant == null) {
            return null;
        }
        RestaurantDTO restaurantDTO = new RestaurantDTO();
        restaurantDTO.setId(restaurant.getId());
        restaurantDTO.setAdditionalCharges(restaurant.getAdditionalCharges());
        restaurantDTO.setAddress(restaurant.getAddress());
        restaurantDTO.setDeliveryInfo(restaurant.getDeliveryInfo());
        if (showGroupOrderList) {
            restaurantDTO.setGroupOrders(groupOrderMapper.mapToDtoList(restaurant.getGroupOrders()));
        }
        restaurantDTO.setMenuUrl(restaurant.getMenuUrl());
        restaurantDTO.setName(restaurant.getName());
        restaurantDTO.setPhone(restaurant.getPhone());
        restaurantDTO.setEmail(restaurant.getEmail());

        return restaurantDTO;
    }

    public Restaurant mapToEntity(RestaurantDTO restaurantDTO, boolean showGroupOrderList) {
        if (restaurantDTO == null) {
            return null;
        }
        Restaurant restaurant = new Restaurant();
        restaurant.setAdditionalCharges(restaurantDTO.getAdditionalCharges());
        restaurant.setAddress(restaurantDTO.getAddress());
        if (showGroupOrderList) {
            restaurant.setGroupOrders(groupOrderMapper.mapToEntityList(restaurantDTO.getGroupOrders()));
        }
        restaurant.setMenuUrl(restaurantDTO.getMenuUrl());
        restaurant.setName(restaurantDTO.getName());
        restaurant.setPhone(restaurantDTO.getPhone());
        restaurant.setId(restaurantDTO.getId());
        restaurant.setDeliveryInfo(restaurantDTO.getDeliveryInfo());
        restaurant.setEmail(restaurantDTO.getEmail());

        return restaurant;
    }

    public List<RestaurantDTO> mapToDtoList(List<Restaurant> restaurants) {
        List<RestaurantDTO> restaurantDTOs = new ArrayList<>();
        for (Restaurant restaurant :
                restaurants) {
            restaurantDTOs.add(mapToDto(restaurant,false));
        }
        return restaurantDTOs;
    }

    public List<Restaurant> mapToEntityList(List<RestaurantDTO> restaurantDTOs) {
        List<Restaurant> restaurants = new ArrayList<>();
        for (RestaurantDTO restaurantDTO :
                restaurantDTOs) {
            restaurants.add(mapToEntity(restaurantDTO,false));
        }
        return restaurants;
    }

}
