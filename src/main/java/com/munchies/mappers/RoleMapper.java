package com.munchies.mappers;

import com.munchies.models.dtos.RoleDTO;
import com.munchies.models.entities.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class RoleMapper {

    @Autowired
    private UserMapper userConverter;

    public RoleDTO mapToDto(Role role, boolean mapUsers) {
        if (role == null) {
            return null;
        }
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(role.getId());
        roleDTO.setRole(role.getRole());
        if (mapUsers) {
            roleDTO.setUsers(userConverter.mapToDtoList(role.getUsers()));
        }

        return roleDTO;
    }

    public Role mapToEntity(RoleDTO roleDTO, boolean mapUsers) {
        if (roleDTO == null) {
            return null;
        }
        Role role = new Role();
        role.setId(roleDTO.getId());
        role.setRole(roleDTO.getRole());
        if (mapUsers) {
            role.setUsers(userConverter.mapToEntityList(roleDTO.getUsers()));
        }

        return role;
    }

    public Set<RoleDTO> mapToDtoList(Set<Role> roles) {
        Set<RoleDTO> roleDTOs = new HashSet<>();
        for (Role role :
                roles) {
            roleDTOs.add(mapToDto(role,false));
        }
        return roleDTOs;
    }

    public Set<Role> mapToEntityList(Set<RoleDTO> roleDTOs) {
        Set<Role> roles = new HashSet<>();
        for (RoleDTO roleDTO :
                roleDTOs) {
            roles.add(mapToEntity(roleDTO,false));
        }
        return roles;
    }
}
