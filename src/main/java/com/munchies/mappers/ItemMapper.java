package com.munchies.mappers;

import com.munchies.models.dtos.ItemDTO;
import com.munchies.models.entities.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ItemMapper {

    @Autowired
    private GroupOrderMapper groupOrderMapper;

//    public ItemDTO mapToDto(Item item) {
//        ItemDTO itemDTO = new ItemDTO();
//
//        if (item == null) {
//            return null;
//        }
//            itemDTO.setCreator(item.getCreator());
//            itemDTO.setGroupOrder(groupOrderMapper.mapToDto(item.getGroupOrder()));
//            itemDTO.setId(item.getId());
//            itemDTO.setItemName(item.getItemName());
//            itemDTO.setPrice(item.getPrice());
//
//        return itemDTO;
//    }
//
//    public Item mapToEntity(ItemDTO itemDTO) {
//        Item item = new Item();
//        if (itemDTO == null) {
//            return null;
//        }
//            item.setCreator(itemDTO.getCreator());
//            item.setGroupOrder(groupOrderMapper.mapToEntity(itemDTO.getGroupOrder()));
//            item.setId(itemDTO.getId());
//            item.setItemName(itemDTO.getItemName());
//            item.setPrice(itemDTO.getPrice());
//
//        return item;
//    }

    public ItemDTO mapToDto(Item item, boolean mapGroupOrder) {
        if (item == null) {
            return null;
        }
        ItemDTO itemDTO = new ItemDTO();
        itemDTO.setCreator(item.getCreator());
        if (mapGroupOrder) {
            itemDTO.setGroupOrder(groupOrderMapper.mapToDto(item.getGroupOrder(), false, false));
        }
        itemDTO.setId(item.getId());
        itemDTO.setItemName(item.getItemName());
        itemDTO.setPrice(item.getPrice());

        return itemDTO;
    }

    public Item mapToEntity(ItemDTO itemDTO, boolean mapGroupOreder) {

        if (itemDTO == null) {
            return null;
        }
        Item item = new Item();

        item.setCreator(itemDTO.getCreator());
        if (mapGroupOreder) {
            item.setGroupOrder(groupOrderMapper.mapToEntity(itemDTO.getGroupOrder(), false, false));
        }
        item.setId(itemDTO.getId());
        item.setItemName(itemDTO.getItemName());
        item.setPrice(itemDTO.getPrice());

        return item;
    }

    public List<ItemDTO> mapToDtoList(List<Item> items) {
        List<ItemDTO> itemDTOs = new ArrayList<>();
        for (Item item :
                items) {
            itemDTOs.add(mapToDto(item, true));
        }
        return itemDTOs;
    }

    public List<Item> mapToEntityList(List<ItemDTO> itemDTOs) {
        List<Item> items = new ArrayList<>();
        for (ItemDTO itemDTO :
                itemDTOs) {
            items.add(mapToEntity(itemDTO, true));
        }
        return items;
    }
}
