package com.munchies.repo;

import com.munchies.models.entities.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RestaurantRepo extends JpaRepository<Restaurant,Integer> {
    Restaurant findByName(String name);
    Restaurant findByPhone(String phone);
    Restaurant findByEmail(String email);


}
