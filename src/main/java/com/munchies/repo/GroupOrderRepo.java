package com.munchies.repo;

import com.munchies.models.dtos.GroupOrderDTO;
import com.munchies.models.dtos.RestaurantDTO;
import com.munchies.models.entities.GroupOrder;
import com.munchies.models.entities.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupOrderRepo extends JpaRepository<GroupOrder,Integer> {
    GroupOrder findByRestaurant(Restaurant restaurant);

}
