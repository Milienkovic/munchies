package com.munchies;

import com.munchies.repo.RoleRepo;
import com.munchies.services.StorageService;
import com.munchies.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MunchiesApplication implements CommandLineRunner {

@Autowired
private StorageService storageService;
    public static void main(String[] args) {
        SpringApplication.run(MunchiesApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        storageService.init();
    }
}
