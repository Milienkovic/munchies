package com.munchies.services.impl;

import com.munchies.models.dtos.RestaurantDTO;
import com.munchies.services.RestaurantService;
import com.munchies.util.StorageProperties;
import com.munchies.services.exceptions.FileSizeException;
import com.munchies.services.exceptions.StorageException;
import com.munchies.services.exceptions.StorageFileNotFoundException;
import com.munchies.services.StorageService;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.MalformedURLException;

import java.nio.file.*;

import java.util.stream.Stream;

@Service
public class StorageServiceImpl implements StorageService {

    @Autowired
    private StorageProperties storageProperties;

    private final Path rootLocation;


    @Autowired
    public StorageServiceImpl(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    @Override
    public void init() {
        try {
            if (!Files.exists(rootLocation)) {
                Files.createDirectories(rootLocation);
            }

        } catch (IOException e) {
            throw new StorageException("could not initialize storage", e);
        }
    }

    @Override
    public void store(MultipartFile file, RestaurantDTO restaurantDTO) throws IOException {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        String extension = FilenameUtils.getExtension(filename);
        if (file.isEmpty()) {
            throw new StorageException("failed to store empty file" + filename);
        }
        if (filename.contains("..")) {
            throw new StorageException("Cannot store file with relative path outside current directory " + filename);
        }
        if (file.getSize() >= storageProperties.getMaxFileSize()) {
            throw new FileSizeException("File is larger than " + storageProperties.getMaxFileSize());
        }
        filename = restaurantDTO.getName() + "_menu." + extension;
        InputStream inputStream = file.getInputStream();
        Files.copy(inputStream, this.rootLocation.resolve(filename), StandardCopyOption.REPLACE_EXISTING);

    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.rootLocation, 1).filter(path -> !path.equals(this.rootLocation)).map(this.rootLocation::relativize);
        } catch (IOException e) {
            throw new StorageException("failed to read stored files", e);
        }
    }

    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new StorageFileNotFoundException("could not read file:" + filename);
            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("could not read file:" + filename, e);
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    @Override
    public void delete(String filename) {
        Path path = rootLocation.resolve(filename);
        File file = path.toFile();
        file.delete();
    }

    @Override
    public void write(MultipartFile file, Path path) throws IOException {
        byte[] bytes;
        if (!file.isEmpty()) {
            bytes = file.getBytes();
            Files.write(path, bytes);
        }
    }

    @Override
    public void download(OutputStream stream, String fileName) {

        String dataDirectory = rootLocation.toString();
        Path file = Paths.get(dataDirectory, fileName);
        if (Files.exists(file)) {
            try {
                Files.copy(file, stream);
                stream.flush();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

}
