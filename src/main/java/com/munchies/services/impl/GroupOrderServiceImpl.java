package com.munchies.services.impl;

import com.munchies.models.dtos.GroupOrderDTO;
import com.munchies.models.dtos.RestaurantDTO;
import com.munchies.models.entities.GroupOrder;
import com.munchies.repo.GroupOrderRepo;
import com.munchies.services.GroupOrderService;
import com.munchies.services.RestaurantService;
import com.munchies.mappers.GroupOrderMapper;
import com.munchies.mappers.RestaurantMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class GroupOrderServiceImpl implements GroupOrderService {
    @Autowired
    private GroupOrderRepo repo;

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private GroupOrderMapper groupOrderMapper;


    @Override
    public List<GroupOrderDTO> findAll() {
        List<GroupOrderDTO> groupOrderDTOs = new ArrayList<>();
        for (GroupOrder groupOrder :
                repo.findAll()) {
            groupOrderDTOs.add(groupOrderMapper.mapToDto(groupOrder, false, false));
        }
        return groupOrderDTOs;
    }

    @Override
    @Transactional
    public GroupOrderDTO findById(int id) {
        GroupOrder groupOrder = repo.findById(id).orElse(null);
        GroupOrderDTO groupOrderDTO = groupOrderMapper.mapToDto(groupOrder, true, true);
        return groupOrderDTO;
    }

    @Override
    public GroupOrderDTO findOrderOnlyById(int id) {
        GroupOrder groupOrder = repo.findById(id).orElse(null);
        GroupOrderDTO groupOrderDTO = groupOrderMapper.mapToDto(groupOrder, false, false);
        return groupOrderDTO;
    }

    @Override
    @Transactional
    public GroupOrderDTO findOne(int id) {
        return groupOrderMapper.mapToDto(repo.findById(id).orElse(null), false, true);
    }

    @Override
    public GroupOrderDTO saveOrUpdate(GroupOrderDTO groupOrderDTO) {
        if (groupOrderDTO.getId() == 0) {
            groupOrderDTO.setTimestamp(new Timestamp(System.currentTimeMillis()));
        }
        GroupOrder groupOrder = repo.save(groupOrderMapper.mapToEntity(groupOrderDTO, true, false));
        return groupOrderMapper.mapToDto(groupOrder, true, false);
    }

    @Override
    public GroupOrderDTO saveOrUpdate(GroupOrderDTO groupOrderDTO, RestaurantDTO restaurantDTO) {
        if (groupOrderDTO.getId() == 0) {
            groupOrderDTO.setTimestamp(new Timestamp(System.currentTimeMillis()));
        }
        groupOrderDTO.setRestaurant(restaurantDTO);
        GroupOrder groupOrder = repo.save(groupOrderMapper.mapToEntity(groupOrderDTO, true, false));
        return groupOrderMapper.mapToDto(groupOrder, true, false);
    }

    @Override
    public void delete(int id) {
        repo.deleteById(id);
    }

    @Override
    @Transactional
    public List<GroupOrderDTO> activeOrders(int restaurantId) {
        RestaurantDTO restaurantDTO = restaurantService.findById(restaurantId);
        Calendar timeoutExpire = Calendar.getInstance();
        Calendar currentTime = Calendar.getInstance();
        currentTime.getTime();
        List<GroupOrderDTO> activeOrders = new ArrayList<>();
        for (int i = 0; i < restaurantDTO.getGroupOrders().size(); i++) {
            timeoutExpire.setTime(restaurantDTO.getGroupOrders().get(i).getTimestamp());
            timeoutExpire.add(Calendar.MINUTE, Integer.valueOf(restaurantDTO.getGroupOrders().get(i).getTimeout()));

            if (timeoutExpire.after(currentTime)) {
                GroupOrder groupOrder = repo.findById(restaurantDTO.getGroupOrders().get(i).getId()).orElse(null);
                activeOrders.add(groupOrderMapper.mapToDto(groupOrder, true, true));
            }
        }
        return activeOrders;
    }


    @Override
    public Long timer(int id) {
        GroupOrder order = repo.findById(id).orElse(null);
        GroupOrderDTO groupOrderDTO = groupOrderMapper.mapToDto(order, false, false);
        Calendar timeoutExpire = Calendar.getInstance();
        Calendar currentTime = Calendar.getInstance();
        currentTime.getTime();
        timeoutExpire.setTime(groupOrderDTO.getTimestamp());
        timeoutExpire.add(Calendar.MINUTE, Integer.parseInt(order.getTimeout()));
        Long time = timeoutExpire.getTimeInMillis() - currentTime.getTimeInMillis();
        return time;
    }


}
