package com.munchies.services.impl.mail;

import com.munchies.services.impl.mail.runnable.MyTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class OrderEmailScheduler {

    @Autowired
    private MyTask myTask;

    TaskScheduler scheduler;

//    @Scheduled(fixedDelay = 30000)
    public void sendOrders() {
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        scheduler = new ConcurrentTaskScheduler(executorService);
        scheduler.schedule(myTask, new Date());

    }
}
