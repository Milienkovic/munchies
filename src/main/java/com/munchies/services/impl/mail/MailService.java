package com.munchies.services.impl.mail;

import com.munchies.models.dtos.GroupOrderDTO;
import com.munchies.services.GroupOrderService;
import com.munchies.services.exceptions.MailAlreadySentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.InternetAddress;
import java.time.Instant;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Service
public class MailService {

    @Autowired
    private TemplateEngine engine;

    @Autowired
    private JavaMailSender sendMail;

    @Autowired
    private GroupOrderService groupOrderService;

    public MimeMessagePreparator constructOrderEmail(GroupOrderDTO order) {
        Context context = new Context();
        context.setVariable("order", order);
        InternetAddress address = new InternetAddress();
        address.setAddress(order.getCreator() + "@munchies.com");

        String text = engine.process("emailTemplate", context);
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper email = new MimeMessageHelper(mimeMessage);
            email.setTo(order.getRestaurant().getEmail());
            email.setSubject("Order Confirmation - " + order.getId());
            email.setText(text, true);
            email.setFrom(address);
        };
        return messagePreparator;
    }

    @Async()
    public void sendMail(MimeMessagePreparator mimeMessagePreparator, int id) {
        GroupOrderDTO groupOrderDTO = groupOrderService.findById(id);
        if (groupOrderService.timer(id) <= 0 && !groupOrderDTO.isSent() && groupOrderDTO.getItems().size() > 0) {
            groupOrderDTO.setSent(true);
            sendMail.send(mimeMessagePreparator);
            groupOrderService.saveOrUpdate(groupOrderDTO);
        } else {
//            throw new MailAlreadySentException("mail has been already sent");
        }
    }

    @Async()
    public void sendAutoMail(int id) {
        GroupOrderDTO groupOrderDTO = groupOrderService.findById(id);

        Runnable runnable = () -> {
            GroupOrderDTO groupOrderDTO1 = groupOrderService.findById(id);
            MimeMessagePreparator mimeMessagePreparator = constructOrderEmail(groupOrderDTO1);
            sendMail(mimeMessagePreparator, id);
        };

        TaskScheduler scheduler;
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        scheduler = new ConcurrentTaskScheduler(executorService);
        Date date = Date.from(Instant.now().plusMillis(groupOrderService.timer(groupOrderDTO.getId())).plusMillis(1000));

        if (groupOrderDTO.isAutoSend() && !groupOrderDTO.isSent()) {
            scheduler.schedule(runnable, date);
        }
    }
}

