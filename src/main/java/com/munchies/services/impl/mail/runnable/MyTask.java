package com.munchies.services.impl.mail.runnable;

import com.munchies.mappers.GroupOrderMapper;
import com.munchies.models.dtos.GroupOrderDTO;
import com.munchies.models.dtos.RestaurantDTO;
import com.munchies.services.GroupOrderService;
import com.munchies.services.RestaurantService;
import com.munchies.services.impl.mail.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;


@Component()
public class MyTask implements Runnable {

    @Autowired
    private GroupOrderService groupOrderService;

    @Autowired
    private MailService mailService;

    @Autowired
    private RestaurantService restaurantService;

    @Override
    public void run() {
        for (RestaurantDTO restaurantDTO :
                restaurantService.findAll()) {
            if (!restaurantDTO.getGroupOrders().isEmpty() || restaurantDTO.getGroupOrders()!=null){
                for (GroupOrderDTO groupOrderDTO :
                        restaurantDTO.getGroupOrders()) {
                    GroupOrderDTO groupOrderDTO1=  groupOrderService.findById(groupOrderDTO.getId());
                    if ((!groupOrderDTO1.getItems().isEmpty() || groupOrderDTO1.getItems()!=null) && !groupOrderDTO1.isSent() ){
                        MimeMessagePreparator mimeMessagePreparator = mailService.constructOrderEmail(groupOrderDTO1);
                        mailService.sendMail(mimeMessagePreparator,groupOrderDTO1.getId());
                    }
                }
            }
        }
    }
}

