package com.munchies.services.impl;

import com.munchies.mappers.GroupOrderMapper;
import com.munchies.models.dtos.GroupOrderDTO;
import com.munchies.models.dtos.RestaurantDTO;
import com.munchies.models.entities.Restaurant;
import com.munchies.repo.RestaurantRepo;
import com.munchies.services.GroupOrderService;
import com.munchies.services.RestaurantService;
import com.munchies.mappers.RestaurantMapper;
import com.munchies.services.StorageService;
import com.munchies.util.GeneralUtil;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class RestaurantServiceImpl implements RestaurantService {

    @Autowired
    private RestaurantRepo repo;

    @Autowired
    private GroupOrderService groupOrderService;

    @Autowired
    private RestaurantMapper restaurantMapper;

    @Autowired
    private StorageService storageService;

    @Override
    @Transactional
    public List<RestaurantDTO> findAll() {
        List<RestaurantDTO> restaurantDTOs = new ArrayList<>();
        for (Restaurant restaurant :
                repo.findAll()) {
            restaurantDTOs.add(restaurantMapper.mapToDto(restaurant, true));
        }
        return restaurantDTOs;
    }

    @Override
    @Transactional
    public RestaurantDTO findById(int id) {
        Restaurant restaurant = repo.findById(id).orElse(null);
        return restaurantMapper.mapToDto(restaurant, true);
    }

    @Override
    @Transactional
    public RestaurantDTO findOne(int id) {
        return restaurantMapper.mapToDto(repo.findById(id).orElse(null), true);
    }

    @Override
    public RestaurantDTO saveOrUpdate(RestaurantDTO restaurantDTO) {
        repo.save(restaurantMapper.mapToEntity(restaurantDTO, false));
        return restaurantDTO;
    }

    @Override
    public RestaurantDTO saveOrUpdate(RestaurantDTO restaurantDTO, MultipartFile file) throws IOException {
        RestaurantDTO oldRestaurantDTO = restaurantMapper.mapToDto(repo.findById(restaurantDTO.getId()).orElse(null), false);
        if (restaurantDTO.getId() != 0 && file.isEmpty()) {
            restaurantDTO.setMenuUrl(oldRestaurantDTO.getMenuUrl());
        }
        if (restaurantDTO.getId() != 0 && !file.isEmpty()) {
            String extension = FilenameUtils.getExtension(file.getOriginalFilename());
            String oldExtension = oldRestaurantDTO.getMenuUrl().substring(oldRestaurantDTO.getMenuUrl().lastIndexOf('.'));
            if (!extension.equalsIgnoreCase(oldExtension)) {
                storageService.delete(oldRestaurantDTO.getMenuUrl());
            }
        }

        if (!file.isEmpty()) {
            storageService.store(file, restaurantDTO);
            restaurantDTO.setMenuUrl(restaurantDTO.getName() + "_" + "menu." + FilenameUtils.getExtension(file.getOriginalFilename()));
        }
        repo.save(restaurantMapper.mapToEntity(restaurantDTO, false));
        return restaurantDTO;
    }

    @Override
    public void deleteById(int id) {
        repo.deleteById(id);
    }

    @Override
    @Transactional
    public RestaurantDTO findByName(String name) {
        Restaurant restaurant = repo.findByName(name);
        return restaurantMapper.mapToDto(restaurant, true);
    }

    @Override
    public List<GroupOrderDTO> activeOrders(int restaurantId) {
        return groupOrderService.activeOrders(restaurantId);
    }

    @Override
    public RestaurantDTO findByPhone(String phone) {
        return restaurantMapper.mapToDto(repo.findByPhone(phone), false);
    }

    public List<Integer> getDeliveryTimeList() {
        List<Integer> list = GeneralUtil.timeList();
        return list;
    }

    @Override
    @Transactional
    public RestaurantDTO findByEmail(String email) {
        return restaurantMapper.mapToDto(repo.findByEmail(email), true);
    }
}
