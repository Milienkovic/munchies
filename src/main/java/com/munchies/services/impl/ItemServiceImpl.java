package com.munchies.services.impl;

import com.munchies.models.dtos.GroupOrderDTO;
import com.munchies.models.dtos.ItemDTO;
import com.munchies.models.entities.Item;
import com.munchies.repo.ItemRepo;
import com.munchies.services.ItemService;
import com.munchies.mappers.ItemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemRepo repo;

    @Autowired
    private ItemMapper itemMapper;

    @Override
    public List<ItemDTO> findAll() {
        List<ItemDTO>itemDTOs = new ArrayList<>();
        for (Item item :
                repo.findAll()) {
            itemDTOs.add(itemMapper.mapToDto(item,true));
        }
        return itemDTOs;
    }

    @Override
    public ItemDTO findById(int id) {
        Item item = repo.findById(id).orElse(null);

        return itemMapper.mapToDto(item,true);
    }

    @Override
    public ItemDTO saveOrUpdate(ItemDTO item) {
        repo.save(itemMapper.mapToEntity(item,true));
        return item;
    }
@Override
    public ItemDTO saveOrUpdate(ItemDTO item, GroupOrderDTO groupOrder) {
        item.setGroupOrder(groupOrder);
        repo.save(itemMapper.mapToEntity(item,true));
        return item;
    }

    @Override
    public void deleteById(int id) {
        repo.deleteById(id);
    }
}
