package com.munchies.services.impl;

import com.munchies.models.dtos.RoleDTO;
import com.munchies.repo.RoleRepo;
import com.munchies.services.RoleService;
import com.munchies.mappers.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public RoleDTO findByRole(String role) {

        return roleMapper.mapToDto(roleRepo.findByRole(role),false);
    }

    @Override
    public RoleDTO findById(int id) {
        return roleMapper.mapToDto(roleRepo.findById(id).orElse(null),false);
    }
}
