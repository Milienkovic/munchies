package com.munchies.services.impl;

import com.munchies.models.dtos.RoleDTO;
import com.munchies.models.dtos.UserDTO;
import com.munchies.models.entities.Role;
import com.munchies.models.entities.User;
import com.munchies.repo.RoleRepo;
import com.munchies.repo.UserRepo;
import com.munchies.services.UserService;
import com.munchies.mappers.RoleMapper;
import com.munchies.mappers.UserMapper;
import com.munchies.util.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public List<UserDTO> findAll() {
        List<UserDTO> userDTOs = new ArrayList<>();
        for (User user :
                userRepo.findAll()) {
            userDTOs.add(userMapper.mapToDto(user,true));
        }
        return userDTOs;
    }

    @Override
    public UserDTO findById(int id) {
        return userMapper.mapToDto(userRepo.findById(id).orElse(null),true);
    }

    @Override
    public UserDTO saveOrUpdate(UserDTO user) {
        userRepo.save(userMapper.mapToEntity(user,true));

        return user;
    }

    @Override
    public void deleteById(int id) {
        userRepo.deleteById(id);
    }

    @Override
    public UserDTO findByEmail(String email) {

        return userMapper.mapToDto(userRepo.findByEmail(email),true);
    }

    //    @Override
//    @Transactional
//    public UserDTO createUser(UserDTO user, Set<RoleDTO> userRoles) throws Exception {
//        UserDTO u = userMapper.mapToDto(userRepo.findByEmail(user.getEmail()));
//        if (u != null) {
//            throw new Exception("email already exists");
//        } else {
//            for (RoleDTO roleDTO :
//                    userRoles) {
//                Role role =roleMapper.mapToEntity(roleDTO);
//               roleRepo.save(role);
//            }
//            user.getUserRoles().addAll(userRoles);
//
//            u = userMapper.mapToDto((userRepo.save(userMapper.mapToEntity(user))));
//        }
//        return u;
//    }
    @Override
    public void saveUser(UserDTO user) {
        user.setPassword(SecurityUtil.passwordEncoder().encode(user.getPassword()));
        RoleDTO userRole = roleMapper.mapToDto(roleRepo.findByRole("EMPLOYEE"),true);
//        if(userRole == null) {
//            RoleDTO role = roleMapper.mapToDto(roleRepo.getOne(1));
//            role.setRole("ADMIN");
//            role.setId(1);
//            roleRepo.save(roleMapper.mapToEntity(role));
//            user.addUserRole(userRole);
//        }else {
//        }
        user.addUserRole(userRole);
        userRepo.save(userMapper.mapToEntity(user,true));
    }
}
