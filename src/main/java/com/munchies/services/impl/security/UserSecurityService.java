package com.munchies.services.impl.security;

import com.munchies.models.dtos.UserDTO;
import com.munchies.repo.UserRepo;
import com.munchies.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserSecurityService implements UserDetailsService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDTO user = userMapper.mapToDto(userRepo.findByEmail(username),true);

        if (user == null){
            throw  new UsernameNotFoundException("Username Doesn't Exists");
        }

        return  new MyUserDetails(user);
    }

    //            (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()
}
