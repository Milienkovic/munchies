package com.munchies.services.impl.security;

import com.munchies.models.dtos.RoleDTO;
import com.munchies.models.dtos.UserDTO;
import com.munchies.models.entities.Role;
import com.munchies.models.entities.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

// !!!! Returns email instead of username
// all flags set to true
public class MyUserDetails implements UserDetails {

    private UserDTO user;

    public MyUserDetails(UserDTO user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> authoritySet = new HashSet<>();
        for (RoleDTO userRole:user.getUserRoles()) {
            authoritySet.add(new Authority(userRole.getRole()));
        }
        return authoritySet;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }
}
