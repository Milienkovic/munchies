package com.munchies.services;

import com.munchies.models.dtos.GroupOrderDTO;
import com.munchies.models.dtos.RestaurantDTO;
import com.munchies.models.entities.GroupOrder;
import com.munchies.models.entities.Restaurant;

import java.util.List;

public interface GroupOrderService {
    List<GroupOrderDTO> findAll();
    GroupOrderDTO findById(int id);

    GroupOrderDTO findOrderOnlyById(int id);

    GroupOrderDTO findOne(int id);
    GroupOrderDTO saveOrUpdate(GroupOrderDTO groupOrder);
    GroupOrderDTO saveOrUpdate(GroupOrderDTO groupOrderDTO, RestaurantDTO restaurantDTO);
    void delete(int id);
    List<GroupOrderDTO> activeOrders(int restaurantId);
//    List<GroupOrderDTO> activeOrders(RestaurantDTO restaurantDTO);
    Long timer(int orderId);

}
