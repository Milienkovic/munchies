package com.munchies.services;

import com.munchies.models.dtos.GroupOrderDTO;
import com.munchies.models.dtos.ItemDTO;
import com.munchies.models.entities.Item;

import java.util.List;

public interface ItemService {
    List<ItemDTO> findAll();
    ItemDTO findById(int id);
    ItemDTO saveOrUpdate(ItemDTO item);

    ItemDTO saveOrUpdate(ItemDTO item, GroupOrderDTO groupOrder);

    void deleteById(int id);

}
