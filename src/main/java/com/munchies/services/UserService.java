package com.munchies.services;

import com.munchies.models.dtos.RoleDTO;
import com.munchies.models.dtos.UserDTO;
import com.munchies.models.entities.Role;
import com.munchies.models.entities.User;

import java.util.List;
import java.util.Set;

public interface UserService {
    List<UserDTO> findAll();
    UserDTO findById(int id);
    UserDTO saveOrUpdate(UserDTO user);
    void deleteById(int id);
    UserDTO findByEmail(String email);
//    UserDTO createUser(UserDTO user, Set<RoleDTO> userRoles) throws Exception;
    void saveUser(UserDTO user);

}
