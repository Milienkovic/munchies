package com.munchies.services;

import com.munchies.models.dtos.GroupOrderDTO;
import com.munchies.models.dtos.RestaurantDTO;
import com.munchies.models.entities.Restaurant;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface RestaurantService {
    List<RestaurantDTO> findAll();
    RestaurantDTO findById(int id);
    RestaurantDTO findOne(int id);
    RestaurantDTO saveOrUpdate(RestaurantDTO restaurant);

    RestaurantDTO saveOrUpdate(RestaurantDTO restaurantDTO, MultipartFile file) throws IOException;

    void deleteById(int id);
    RestaurantDTO findByName(String name);

    List<GroupOrderDTO> activeOrders(int id);
    RestaurantDTO findByPhone(String phone);
    List<Integer> getDeliveryTimeList();
    RestaurantDTO findByEmail(String email);
}
