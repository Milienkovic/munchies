package com.munchies.services;

import com.munchies.models.dtos.RestaurantDTO;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {



    void init();
    void store(MultipartFile multipartFile, RestaurantDTO restaurantDTO) throws IOException;
    Stream<Path> loadAll();
    Path load(String filename);
    Resource loadAsResource(String filename);
    void deleteAll();
//    void write(MultipartFile file,Path path) throws IOException;

    void delete(String filename);

    void write(MultipartFile file, Path path) throws IOException;

    void download(OutputStream stream, String fileName);

//    void download(HttpServletResponse response, String fileName);


//    void downloadWithJavaIO(String url, String localFilename);
//
//    void downloadWithJava7IO(String url, String localFilename);
//
//    void downloadWithJavaNIO(String fileURL, String localFilename) throws IOException;
//
//    void downloadWithApacheCommons(String url, String localFilename);
}
