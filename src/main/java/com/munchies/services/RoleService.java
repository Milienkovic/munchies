package com.munchies.services;

import com.munchies.models.dtos.RoleDTO;
import com.munchies.models.entities.Role;

public interface RoleService {
    RoleDTO findByRole(String role);
    RoleDTO findById(int id);
}
